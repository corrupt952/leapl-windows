#define NONE 0
#define EQUIP 1
#define REMOVE 2
#define EXIT 3
#define FAIL 4

#pragma comment(linker, "/Section:.Shared,RWS")
#pragma data_seg(".Shared")
float mX = -1;
float mY = -1;
float mZ = -1;
char mStatusCode = NONE;
#pragma data_seg()

extern "C"
{
	/*--------------------------------------------------------
		Staus

		[Status Code]
		 * None 0
		 * Start 1
		 * Exit 2
		 * Failed
	--------------------------------------------------------*/
	/* Getting */
	__declspec(dllexport) void __stdcall SetStatusCode(char statusCode)
	{
		mStatusCode = statusCode;
	}

	/* Getting status code */
	__declspec(dllexport) char __stdcall GetStatusCode()
	{
		return mStatusCode;
	}

	/*--------------------------------------------------------
		Setting value
	--------------------------------------------------------*/
	/* Setting X vector*/
	__declspec(dllexport) void __stdcall SetX(float x)
	{
		mX = x;
	}

	/* Setting Y vector*/
	__declspec(dllexport) void __stdcall SetY(float y)
	{
		mY = y;
	}

	/* Setting Z vector*/
	__declspec(dllexport) void __stdcall SetZ(float z)
	{
		mZ = z;
	}


	/*--------------------------------------------------------
		Getting value
	--------------------------------------------------------*/
	/* Getting X vector */
	__declspec(dllexport) float __stdcall GetX()
	{
		return mX;
	}

	/* Getting Y vector */
	__declspec(dllexport) float __stdcall GetY()
	{
		return mY;
	}

	/* Getting Y vector */
	__declspec(dllexport) float __stdcall GetZ()
	{
		return mZ;
	}
}