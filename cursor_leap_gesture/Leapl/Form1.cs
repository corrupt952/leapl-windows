﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.ComponentModel;

namespace Leapl
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// LeapMotionで操作可能にさせる
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartLeapl(object sender, EventArgs e)
        {
            LeaplUtil.Equip();
        }

        /// <summary>
        /// LeapMotionでの操作を不可にする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitLeapl(object sender, EventArgs e)
        {
            LeaplUtil.Remove();
        }

        /// <summary>
        /// フォームがロードされているときの処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            // LeaplAPIの初期化
            LeaplUtil.Init();

            // LeaplCoreの起動
            Process.Start(@"LeaplCore.exe");

            // 非表示にする
            this.Visible = false;
        }

        /// <summary>
        /// アプリケーションの終了
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exit(object sender, EventArgs e)
        {
            LeaplUtil.Exit();
            trayMenuStrip.Dispose();
            Application.Exit();
        }
    }
}
