﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Leapl
{
    public class LeaplUtil
    {
        /// <summary>
        /// Status code enum
        /// </summary>
        public enum StatusCode
        {
            NONE, EQUIP, REMOVE, EXIT, FAIL
        }

        /// <summary>
        /// LeaplCoreの初期値の設定する
        /// </summary>
        public static void Init()
        {
            SetStatusCode((char)StatusCode.NONE);
        }

        /// <summary>
        /// LeapMotionで操作可能にさせる
        /// </summary>
        public static void Equip()
        {
            SetStatusCode((char)StatusCode.EQUIP);
        }

        /// <summary>
        /// LeapMotionで操作不可にする
        /// </summary>
        public static void Remove()
        {
            SetStatusCode((char)StatusCode.REMOVE);
        }

        /// <summary>
        /// LeaplCoreを終了させる
        /// </summary>
        public static void Exit()
        {
            SetStatusCode((char)StatusCode.EXIT);
        }

        [DllImport("LeaplAPI.dll", EntryPoint = "SetStatusCode")]
        private extern static void SetStatusCode(char statusCode);
        [DllImport("LeaplAPI.dll", EntryPoint = "GetStatusCode")]
        private extern static char GetStatusCode();
        [DllImport("LeaplAPI.dll", EntryPoint = "GetX")]
        private extern static float GetX();
        [DllImport("LeaplAPI.dll", EntryPoint = "GetY")]
        private extern static float GetY();
        [DllImport("LeaplAPI.dll", EntryPoint = "GetZ")]
        private extern static float GetZ();
    }
}
