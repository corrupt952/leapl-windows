﻿#include "LEAPL_Listener.h"

#include<Windows.h>

using namespace Leap;

void LEAPL_Listener::onConnect(const Leap::Controller& controller){
	controller.enableGesture(Gesture::TYPE_CIRCLE);
}

void LEAPL_Listener::onFrame(const Leap::Controller& controller){
	
	//マウス管理
	ScreenList screens = controller.locatedScreens();
	Frame frame = controller.frame();
	PointableList pointables = frame.pointables();
	if(!screens.isEmpty() && !pointables.isEmpty()){ 
		Screen screen = screens[0];
		InteractionBox ibox = frame.interactionBox();
		Vector fingerPos = ibox.normalizePoint(pointables[0].stabilizedTipPosition());		
		//Vector fingerPos = screen.intersect(pointables[0], true);

		float x = fingerPos.x * screen.widthPixels();
		float y = screen.heightPixels() - fingerPos.y * screen.heightPixels();
		
		SetCursorPos(x, y);
		int touching_count = 0;

		for each(Pointable pointable in pointables){
			if (pointable.touchZone() == Pointable::ZONE_TOUCHING){
				touching_count++;
			}
		}
		//タッチング
		if (touching_count != 0){
			if (flag == 0){ time(&starttime); }
			flag = (touching_count == 1 ? CLICK_LEFT : CLICK_RIGHT);
			time(&midtime);
			if (difftime(midtime, starttime) == 2 && click == NO_CLICKED){
				if (flag == CLICK_LEFT){
					mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
				}
				else if (flag == CLICK_RIGHT){
					mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
				}
				click = CLICKED;
			}
		}
		//ホバーリング
		else{
			time(&endtime);
			if (difftime(endtime, starttime) < 2){
				if (flag == CLICK_LEFT){
					mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
				}
				else if(flag==CLICK_RIGHT){
					mouse_event(MOUSEEVENTF_RIGHTDOWN, 0, 0, 0, 0);
				}
			}
			if (flag == CLICK_LEFT){
				mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
				click = NO_CLICKED;
			}
			else if(flag == CLICK_RIGHT){
				mouse_event(MOUSEEVENTF_RIGHTUP, 0, 0, 0, 0);
				click = NO_CLICKED;
			}
			flag = 0;

			//ジェスチャー管理
			GestureList gestures = frame.gestures();
			for each(Gesture gesture in gestures){
				Pointable pointable = pointables[0];

				//サークル＝マウスホイールコロコロ
				if (gesture.type() == Gesture::TYPE_CIRCLE){
					CircleGesture circle = gesture;
					int circlevec;
					if (circle.pointable().direction().angleTo(circle.normal()) <= PI / 2) {	//時計回り
						circlevec = -1;
					}
					else{																		//反時計回り
						circlevec = 1;
					}
					mouse_event(MOUSEEVENTF_WHEEL, 0, 0, 10 * circlevec, 0);
				}
			}
		}
	}
}