#pragma once

#include<Leap.h>
#include<time.h>

using namespace Leap;

class LEAPL_Listener : public Listener{

public:
	virtual void onConnect(const Leap::Controller&);
	virtual void onFrame(const Leap::Controller&);

private:
	// Variables
	const int CLICK_RIGHT = 2;
	const int CLICK_LEFT = 1;
	const int CLICKED = 1;
	const int NO_CLICKED = 0;
	int flag = 0;
	bool click = 0;
	time_t starttime = 0, midtime = 0, endtime = 0;
};