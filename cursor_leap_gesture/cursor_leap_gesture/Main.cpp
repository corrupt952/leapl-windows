#include "LEAPL_Listener.h"

#include<Windows.h>

#define NONE 0
#define EQUIP 1
#define REMOVE 2
#define EXIT 3
#define FAIL 4

bool equip = false;

/* Add listener */
bool AddListener(Controller &contoller, LEAPL_Listener &listener)
{
	if (!equip)
	{
		equip = contoller.addListener(listener);
	}
	return equip;
}

/* Remove listener */
bool RemoveListener(Controller &controller, LEAPL_Listener &listener)
{
	if (equip)
	{
		equip = !controller.removeListener(listener);
	}
	return !equip;
}

/* Main */
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int){
	HMODULE hDLL;
	bool run = true;
	LEAPL_Listener listener;
	Controller controller;
	controller.setPolicyFlags(Leap::Controller::POLICY_BACKGROUND_FRAMES);

	// Loading dll
	hDLL = LoadLibrary(L"LeaplAPI.dll");
	if (hDLL != NULL)
	{
		char (*GetStatusCode)();
		void (*SetStatusCode)(char statusCode);
		// DLLを読み込めた場合
		if ((GetStatusCode = (char (*)())GetProcAddress(hDLL, "GetStatusCode"))
			&& (SetStatusCode = (void (*)(char))GetProcAddress(hDLL, "SetStatusCode")))
		{
			while (run)
			{
				
				switch (GetStatusCode())
				{
				case NONE:
					// 何もしない
					break;
				case EQUIP:
					AddListener(controller, listener);
					if (equip)
					{
						SetStatusCode(NONE);
					}
					else
					{
						SetStatusCode(FAIL);
					}
					break;
				case REMOVE:
					RemoveListener(controller, listener);
					if (!equip)
					{
						SetStatusCode(NONE);
					}
					else
					{
						SetStatusCode(FAIL);
					}
					break;
				case EXIT:
					run = false;
					SetStatusCode(NONE);
					break;
				}
				System::Threading::Thread::Sleep(1000);
			}
		}
		else
		{
			// TODO: ここのエラーをどうするか考える
		}
	}
	else
	{
		// TODO: ここのエラーをどうするか考える
	}

	// Removing listener
	RemoveListener(controller, listener);
	return 0;
}